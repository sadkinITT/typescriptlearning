class Person {
    firstName: string;
    lastName: string;

    greet() {
        console.log("Hey there");
    }
}

class Programmer extends Person {
    
    greet() {
        console.log("Hellow world");
    }

    greetLikeNormalPeople() {
        this.greet();
    }

    constructor() {
        super();
    }

    greetLikeOldPeople() {
        super.greet();
    }
}

var aProgrammer = new Programmer();

aProgrammer.greet();