function echo(arg:any) : any{
    return arg;
}

let astring : string = echo(1);

class Student {
    firstName: string;
    lastName: string;

    constructor(firstName: string, lastName: string) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    getName() : string {
        return this.firstName + " " + this.lastName;
    }
}

class SectionA extends Student {

}

class SectionB extends Student {

}

let s1 = new SectionA("Kingsuk","Das");
let s2 = new SectionB("Mita","Das");

// accepts any type and returns any type
function studentEcho(s : any) : any {
    return s;
}

var secAroll1 = studentEcho(s1);
var secBroll1 = studentEcho(s2);

//if wants to return only Student type
function anotherStudentEcho(s : any) : Student {
    return s;
}

var secAroll2 = anotherStudentEcho(s1); // here we are losing type from SectionA to Student
var secBroll2 = anotherStudentEcho(s2); // here we are losing type from SectionB to Student

// we dont want to lose type , also want to restrict to Student type
function genericStudentEcho<T extends Student>(s:T) : T {
    return s;
}

var secAroll3 = genericStudentEcho(s1);
var secBroll3 = genericStudentEcho(s2);
