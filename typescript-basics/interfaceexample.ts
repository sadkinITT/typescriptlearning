interface IPerson {
    firstName: string;
    lastName: string;
    getName(): string;
}

class Foo implements IPerson {
    firstName: string;
    lastName: string;
    getName(): string {
        return this.firstName + this.lastName;
    }
    
}

let aPerson : IPerson = new Foo();
aPerson.getName();

//duck typing
let myobj = {
    firstName: "test",
    lastName: "check",
    getName: () => "test check"
}

aPerson = myobj;

