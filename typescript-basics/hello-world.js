var fn = function () { return 'response'; };
var a;
var b;
var c;
var foo;
var n;
var arr;
var arr1 = [1, 2, 3, 4];
var myTuple;
var d;
var e;
a = 10;
b = true;
c = 'Hello';
arr = [1, 2, 3];
arr.push(4);
myTuple = [1, true, "hello"];
//transpiling helps to solve runtime problems which can 
//happen in browser, transpiling will find any wrong type
//usage or wrong declaration or any issues which JS was unable to do
function add(a, b) {
    return a + b;
}
function helloprint(a, b, c) {
    console.log(a + b + c);
}
function hellotypescript(a, b) {
    if (b === void 0) { b = 'typescript'; }
    return a + b;
}
