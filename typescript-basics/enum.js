var daysOfWeek;
(function (daysOfWeek) {
    daysOfWeek[daysOfWeek["SUN"] = 0] = "SUN";
    daysOfWeek[daysOfWeek["MON"] = 1] = "MON";
    daysOfWeek[daysOfWeek["TUE"] = 2] = "TUE";
    daysOfWeek[daysOfWeek["WED"] = 3] = "WED";
    daysOfWeek[daysOfWeek["THU"] = 4] = "THU";
    daysOfWeek[daysOfWeek["FRI"] = 5] = "FRI";
    daysOfWeek[daysOfWeek["SAT"] = 6] = "SAT";
})(daysOfWeek || (daysOfWeek = {}));
var day;
day = daysOfWeek.FRI;
