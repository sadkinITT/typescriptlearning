class Person {
    firstName: string;

    set(value:string) {
        if (value.length >3) {
            this.firstName = value;
        }
        else{
            this.firstName = "";
        }
    }

    get() : string {
        return this.firstName;
    }
}

var p : Person = new Person();

console.log(p.firstName);
p.firstName = "Ma";
console.log(p.firstName);
p.firstName = "Maximilian";
console.log(p.firstName);