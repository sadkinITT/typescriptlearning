var Person = /** @class */ (function () {
    function Person() {
    }
    Person.prototype.set = function (value) {
        if (value.length > 3) {
            this.firstName = value;
        }
        else {
            this.firstName = "";
        }
    };
    Person.prototype.get = function () {
        return this.firstName;
    };
    return Person;
}());
var p = new Person();
console.log(p.firstName);
p.firstName = "Ma";
console.log(p.firstName);
p.firstName = "Maximilian";
console.log(p.firstName);
