var fn = () => 'response';

var a: number;
var b: boolean;
var c: string;
var foo: undefined;
var n: null;
var arr: number[];
var arr1: number[] = [1,2,3,4];
var myTuple: [number,boolean,string];
var d: any;
var e: number | boolean;


a = 10;
b = true;
c = 'Hello';
arr = [1,2,3];
arr.push(4);
myTuple = [1,true,"hello"];

//transpiling helps to solve runtime problems which can 
//happen in browser, transpiling will find any wrong type
//usage or wrong declaration or any issues which JS was unable to do

function add(a,b){
    return a+b;
}

function helloprint(a,b,c?){
    console.log(a+b+c)
}

function hellotypescript(a,b='typescript'){
    return a+b
}