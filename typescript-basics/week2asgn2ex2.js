function greet(name) {
    if (name === undefined) {
        name = "Max";
    }
    console.log("Hello, " + name);
}
greet();
greet("Anna");
