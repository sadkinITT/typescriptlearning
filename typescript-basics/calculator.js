var Calculator = /** @class */ (function () {
    function Calculator() {
    }
    Calculator.prototype.calculate = function (a, b, sign) {
        var n;
        switch (sign) {
            case "+":
                n = a + b;
                break;
            case "-":
                n = a - b;
                break;
            case "*":
                n = a * b;
                break;
            case "/":
                n = a / b;
                break;
        }
        return n;
    };
    return Calculator;
}());
var cal = new Calculator();
var res = cal.calculate(1, 2, "+");
console.log(res);
