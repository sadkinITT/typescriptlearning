var numbers: number[] = [-3,33,38,5];
console.log(Math.min.apply(Math,numbers));

var newArray: number[] = [55,20];
Array.prototype.push.apply(newArray,numbers);
console.log(newArray);

var testResults: number[] = [3.89, 2.99, 1.38];
var result1 = testResults[0];
var result2 = testResults[1];
var result3 = testResults[2];
console.log(result1,result2,result3);

class Scientist {
    firstName: string;
    experience: string;
}

var scientist = new Scientist();
var firstName = scientist.firstName;
var experience = scientist.experience;
console.log(firstName, experience);