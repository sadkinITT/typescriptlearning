class Calculator {

    calculate(a:number,b:number,sign:string) : number {
        var n : number;
        switch (sign) {
        case "+" :
            n = a + b;
            break;
        case "-" :
            n = a - b;
            break;
        case "*" :
            n = a * b;
            break;
        case "/" :
            n = a / b;
            break;
        } 
        return n;
    }
}

var cal = new Calculator();

var res = cal.calculate(1,2,"+");

console.log(res);