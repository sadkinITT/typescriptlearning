var Person = /** @class */ (function () {
    function Person(firstName, lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }
    Person.prototype.getName = function () {
        return this.firstName + " " + this.lastName;
    };
    return Person;
}());
var aPerson = new Person("Kingsuk", "Das");
var anotherPerson = new Person("Mita", "Das");
console.log(aPerson.getName());
console.log(anotherPerson.getName());
