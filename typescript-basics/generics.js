var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
function echo(arg) {
    return arg;
}
var astring = echo(1);
var Student = /** @class */ (function () {
    function Student(firstName, lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }
    Student.prototype.getName = function () {
        return this.firstName + " " + this.lastName;
    };
    return Student;
}());
var SectionA = /** @class */ (function (_super) {
    __extends(SectionA, _super);
    function SectionA() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return SectionA;
}(Student));
var SectionB = /** @class */ (function (_super) {
    __extends(SectionB, _super);
    function SectionB() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return SectionB;
}(Student));
var s1 = new SectionA("Kingsuk", "Das");
var s2 = new SectionB("Mita", "Das");
// accepts any type and returns any type
function studentEcho(s) {
    return s;
}
var secAroll1 = studentEcho(s1);
var secBroll1 = studentEcho(s2);
//if wants to return only Student type
function anotherStudentEcho(s) {
    return s;
}
var secAroll2 = anotherStudentEcho(s1); // here we are losing type from SectionA to Student
var secBroll2 = anotherStudentEcho(s2); // here we are losing type from SectionB to Student
// we dont want to lose type , also want to restrict to Student type
function genericStudentEcho(s) {
    return s;
}
var secAroll3 = genericStudentEcho(s1);
var secBroll3 = genericStudentEcho(s2);
