class Person{
    firstName: string;
    lastName: string;
    
    constructor(firstName:string,lastName:string){
        this.firstName = firstName;
        this.lastName = lastName;
    }


    getName(){
        return this.firstName + " " + this.lastName
    }
}

var aPerson : Person = new Person("Kingsuk","Das");
var anotherPerson = new Person("Mita","Das");

console.log(aPerson.getName());
console.log(anotherPerson.getName());