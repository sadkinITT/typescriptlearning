var Todo = /** @class */ (function () {
    function Todo() {
        this.listsOfWork = new Array(10);
    }
    Todo.prototype.setWork = function (listsOfWork) {
        var len = listsOfWork.length;
        var i = 0;
        for (; i < len; i++) {
            this.listsOfWork[i] = listsOfWork[i];
        }
    };
    Todo.prototype.getWork = function () {
        var i = 0;
        var s = new Array(10);
        for (; i < this.listsOfWork.length; i++) {
            s[i] = this.listsOfWork[i];
        }
        return s;
    };
    return Todo;
}());
var todoObj = new Todo();
var works = ["learn something", "take food properly", "take sleep atleast 8 hours"];
todoObj.setWork(works);
var someworks = todoObj.getWork();
console.log(someworks);
