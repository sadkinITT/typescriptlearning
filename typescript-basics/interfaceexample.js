var Foo = /** @class */ (function () {
    function Foo() {
    }
    Foo.prototype.getName = function () {
        return this.firstName + this.lastName;
    };
    return Foo;
}());
var aPerson = new Foo();
aPerson.getName();
//duck typing
var myobj = {
    firstName: "test",
    lastName: "check",
    getName: function () { return "test check"; }
};
aPerson = myobj;
